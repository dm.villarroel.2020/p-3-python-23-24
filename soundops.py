from mysound import Sound  # importamos

def soundadd(s1: Sound, s2: Sound) -> Sound:

    min_samples = min(len(s1.buffer), len(s2.buffer))

    # Inicializa un nuevo objeto Sound con el mismo período de muestreo
    result_sound = Sound(s1.duration if len(s1.buffer) == min_samples else s2.duration)

    # Realiza la suma de las muestras y almacena el resultado en el nuevo objeto
    result_sound.buffer = [s1.buffer[i] + s2.buffer[i] for i in range(min_samples)]

    return result_sound
