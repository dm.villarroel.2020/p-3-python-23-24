 # EJERCICIO 5
from mysound import Sound # importamos la clase sound del mysound.py

class SoundSin(Sound):
    def __init__(self, duration, frecuency, amplitude,):

        super().__init__(duration) # llama al constructor Sound
        self.sin(frecuency,amplitude) # genera automaticamente la onda

sound_sin = SoundSin(2,440,0.5)

visualizar_onda = sound_sin.bars(0.1)

print(visualizar_onda)