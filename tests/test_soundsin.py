# EJERCICIO 6

import unittest
from mysoundsin import SoundSin

class TestSoundSin(unittest.TestCase):

    def test_init(self):
        # inicializamos
        sound_sin = SoundSin(1, 1100, 0.5)
        self.assertEqual(sound_sin.duration, 1)
        self.assertEqual(sound_sin.samples_second, 44100)
        self.assertEqual(sound_sin.max_amplitude, 32767)  # valor máximo de amplitude para 16 bits
        self.assertIsNotNone(sound_sin.buffer)

    def test_sin_wave_generation(self):
        # probamos la generación de la onda sinusoidal
        sound_sin = SoundSin(1, 1100, 0.5)
        self.assertIsNotNone(sound_sin.buffer)

    def test_bars(self):
        # probamos la generación de la representación de barras
        sound_sin = SoundSin(1, 1100, 0.5)
        bars = sound_sin.bars(0.01)
        self.assertIsNotNone(bars)
        self.assertTrue(isinstance(bars, str))

if __name__ == '__main__':
    unittest.main()
