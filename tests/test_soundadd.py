# EJERCICIO 8
# importamos y cargamos funciones que nos interesan
import unittest
from mysound import Sound
from soundops import soundadd

class TestSoundAdd(unittest.TestCase):

    def test_soundadd(self):
        # creamos dos sonidos de prueba con solo 2 muestras cada uno
        s1 = Sound(1)
        s1.buffer = [1, 2]  # sonido con 2 muestras

        s2 = Sound(1)
        s2.buffer = [3, 4]  # sonido con 2 muestras

        # sumamos los sonidos utilizando la funcion de soundadd
        result_sound = soundadd(s1, s2)

        # verificamos que el resultado sea un objeto Sound
        self.assertIsInstance(result_sound, Sound)

        # verificamos que el resultado tenga la longitud correcta
        self.assertEqual(len(result_sound.buffer), 2)  # Debería tener 2 muestras

        # verificamos que las muestras del resultado sean correctas
        expected_result = [1 + 3, 2 + 4]  # suma de las primeras 2 muestras
        self.assertEqual(result_sound.buffer, expected_result)


if __name__ == '__main__':
    unittest.main()

